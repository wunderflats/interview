# Full stack app
This task requires you to build a simple full stack app and the requirements are:
- build a page that contains header, sidebar, content sections of the page with the requirements:

```
---------------------------------------
|                header               | height:50px
---------------------------------------
|          |                          |
|          |                          |
|  sidebar |         content          | rest of height
|          |                          |
|          |                          |
---------------------------------------
    450px         rest of width
```
- the header should contain title "Wunderflats"
- the sidebar should contain a button which when clicked will fetch the listings data from the server using AJAX and present the data in a list form inside the content section
- build an api that will send the listings data that is inside the listings.csv(comma separated values) file, the structure of the file is id,title,description

## Notes
- The application can be built using any technologies, pre-done repose, boilerplates..., this folder here provides a simple setup using express on the back-end and React on the front-end and it can be used to finish this task
- Feel free to use any library you want but you will have to provide a valid reason why.

## wunderflats boilerplate usage 
- run `npm install` inside the test-app folder
- run `npm start` to start the server
- go to localhost:5001
- the server-side changes will not be auto-reloaded
import http from 'http'
import express from 'express'
import path from 'path'
import bodyParser from 'body-parser'
import winston from 'winston'
import compression from 'compression'
import addWebpackMiddleware from './utils/webpackRoutes'
import renderHtml from './renderHtml'
import { NODE_ENV } from './config'
import pushstate from 'connect-pushstate'

const app = express()

//------------------set up middleware------------------------------------
app.use(compression())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
if (NODE_ENV === 'development') {
  addWebpackMiddleware(app)
} else {
  app.use('/', express.static(path.join(__dirname, '../../public')))
}

//------------------set up api routes------------------------------------

//------------------set up page routes------------------------------------
app.use(pushstate())
app.get('/', (req, res) => {
  res.setHeader('Content-Type', 'text/html')
  res.write(renderHtml())
  res.end()
})

//------------------set up error handler------------------------------------
app.use((req, res, next) => {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})

app.use(function(err, req, res) {
  res.status(err.status || 500)
  res.json({
    message: err.message,
  })
})

if (require.main === module) {
  const port = 5001
  const server = http.createServer(app)
  server.listen(process.env.PORT || port, function() {
    winston.info(`Listening on ${port}`)
  })
}

module.exports = app

import { NODE_ENV } from './config'

export default () => {
  const indexHtml = `<!DOCTYPE html>
  <html>
    <head>
      <link rel="shortcut icon" href="data:image/x-icon;," type="image/x-icon"> 
      <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
      <title>react starter</title>
      ${
        NODE_ENV !== 'development'
          ? '<link href="assets/styles.css" rel="stylesheet">'
          : ''
      }
    </head>
    <body>
      <div id="app">
      </div>
    </body>
    <script type="text/javascript" src="assets/bundle.js"></script>
  </html>`
  return indexHtml
}

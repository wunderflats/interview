const duplicate = require('./index')
test('test duplicate', () => {
  const input = [1, 2, 3, 4, 5]
  const res = duplicate(input)
  expect(res).toEqual([1, 2, 3, 4, 5, 1, 2, 3, 4, 5])
})

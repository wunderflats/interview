const reverse = require('./index')

test("test reverse", () => {
    const res = reverse('Yoyo Tricks')
    expect(res).toEqual('skcirT oyoY')
})
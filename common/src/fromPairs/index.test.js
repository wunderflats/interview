const fromPairs = require('./index')
test('test fromPairs', () => {
    const input = [['a', 1], ['b', 2]];
    const res = fromPairs(input)
    expect(res).toEqual({ a: 1, b: 2 })

})
const multiply = require('./index')

test('test multiply', () => {
  const res = multiply(2)(3)(4)
  expect(res).toEqual(24)
})

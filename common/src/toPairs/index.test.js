const toPairs = require('./index')
test('test toPairs', () => {
    const input = { a: 1, b: 2 }
    const res = toPairs(input)
    expect(res).toEqual([['a', 1], ['b', 2]])
})